package monpackage;

public class Exo1bis 
{

	public static void main(String[] args) 
	{
		// Declaration des variables
		int Nb;
		//Saisie de la variable par l'utilisateur
		Nb = Saisie.lire_int("Quel est votre nombre ?");
		//Traitement
		if (Nb > 0) System.out.println("Votre nombre est positif");
		else System.out.println("Votre nombre est negatif");
	}

}
