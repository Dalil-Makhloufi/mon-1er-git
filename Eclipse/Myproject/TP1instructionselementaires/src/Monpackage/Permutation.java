package Monpackage;

public class Permutation {

	
	public static void main(String[] args) {
		
		
		        // Declarations des variables
				int A, B, C, D, E; 
				// Saisie par l'utilisateur des valeurs a permuter 
				A = Saisie.lire_int("Saisir un premier nombre");
				B = Saisie.lire_int("Saisir un deuxieme nombre");
				C = Saisie.lire_int("Saisir un troisieme nombre");
				D = Saisie.lire_int("Saisir un quatrieme nombre");
				// Affichage es valeurs avant permutation 
				System.out.println(" Avant permutation, la valeur de A est " + A + " et la valeur de B est "+B +" la valeur de C est "+C+"la valeur de D est "+D );
				// Traitement 
				E = A;
				A = B; 
				B = C; 
				C = D;
				D = E ;
		
			 // Affichage des valeurs apres permutation 
				System.out.println(" Apres permutation, la valeur de A est " + A + " et la valeur de B est "+B +" la valeur de C est "+C+"la valeur de D est "+D );

	}

}
