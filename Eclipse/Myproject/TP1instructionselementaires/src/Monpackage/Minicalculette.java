package Monpackage;

import java.awt.desktop.SystemEventListener;

public class Minicalculette {

	public static void main(String[] args) {
		// déclaration des variables
		int X1, X2, X3, P, S;
		double NA;
		// Saisie des 3 valeurs 
		X1 = Saisie.lire_int("Quel est la premiere valeur ?");
		X2 = Saisie.lire_int("Quel est la deuxieme valeur ?");
		X3 = Saisie.lire_int("Quel est la troisieme valeur ?");
		// Calcul
		S = X1 + X2 + X3;
		P = X1 * X2 * X3; 
		NA = S / 3; 
		// Affichage des resultats 
		System.out.println("La somme vaut "+ S +", le produit vaut " + P +" et la moyenne arithmetique vaut "+NA);
		
		
	}

}
