package Monpackage;

public class SurfaceCercle {
	

	public static void main(String[] args) {
		
		
		// déclaration des variables 
		double S;
		int R;
		// déclaration de la constante 
		final double PI = 3.1415;
		// Quéstionnemnt de l'utilisateur 
		R = Saisie.lire_int("Quel est le rayon?");
		// Calcul de la surface 
		S = PI * R * R;
		// Affichage du resultat
		System.out.println("La surface du cercle vaut "+ S);

	}

}
