package Monpackage;

public class Exercice4 {

	public static void main(String[] args) {
		// Declarations des variables
		int A, B, C;
		// Saisir par l'utilisateur des valeurs a permuter 
		A = Saisie.lire_int("Saisir un premier nombre");
		B = Saisie.lire_int("Saisir un deuxieme nombre");
		// Affichage es valeurs avant permutation 
		System.out.println(" Avant permutation, la valeur de A est " + A + " et la valeur de B est "+B);
		// Traitement 
		C=A;
		A=B;
		B=C;
		// Affichage des valeurs apres permutation 
		System.out.println(" Apres permutation, la valeur de A est " + A + " et la valeur de B est "+B);
				
	}

}
